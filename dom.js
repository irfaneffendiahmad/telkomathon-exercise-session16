function ubahStyleHTML() {
    const judul = document.getElementById('judul');
    judul.style.color = 'green';
    judul.style.backgroundColor = 'yellow';
    judul.innerHTML = 'Hello World! Now we learn about JS DOM, Debugging & Linter';
    judul.style.border = '10px solid';
    judul.style.borderBottomColor = 'red';
    judul.style.borderRightColor = 'red';
}