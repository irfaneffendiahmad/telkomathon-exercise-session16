// Part 1 - Object Intro
// Array, adalah variabel yang memiliki key berupa index dan value (nilai);
// Objek, adalah variabel yang memiliki key berupa name/ nama dan value (nilai);

// let myKtp = ['wafa', 17, false, 'jl. jonggrang'];

// let nama = 'Syahrini';
// let umur = '55';
// let alamat = 'jl. Mars';

// let ktpKu = {
//     nama: 'afgan',
//     umur: 17,
//     isMarried: false,
//     alamat: 'jl. jonggrang',
//     arrGpa: [1, 2, 3],
//     greeting() {
//         console.log(`Namaku adalah ${this.nama} dan umurku ${umur} serta alamatku di ${alamat}`);
//     },
//     salam: function() {
//         console.log('tes 123');
//     },
//     sumGpa: function(){
//         let total = 0;
//         let gpa = this.arrGpa;
//         for(let i = 0; i < gpa.length; i++) {
//             // total = total + gpa[i];
//             total += gpa[i]
//         }
//         return total/ gpa.length;
//         // console.log(total/ gpa.length);
//         // alert(total/ gpa.length);
//     },
// };

// nama, umur, isMarried, alamat, gpa, itu semuanya disebut dengan properties.
// greeting disebut dengan method .

// let sumGpa = rata2 gpa; // cluenya, pake for loop/ while.

// Part 2 - Cara membuat object
// 1. Object Literal
// let student1 = {
//     nama: 'afgan',
//     umur: 17,
//     isMarried: false,
//     alamat: {
//         rumah: 'Jl. Jonggrang',
//         kerja: 'Jl. Telkom',
//     },
//     arrGpa: [3.5, 4.0, 4.0],
// }

// let student2 = {
//     nama: 'Syahrini',
//     umur: 50,
//     isMarried: yes,
//     alamat: {
//         rumah: 'Jl. Mars',
//         kerja: 'Jl. Telkom',
//     },
//     arrGpa: [2.5, 1.5, 3.5],
// }

// let student3 = {
//     nama: 'Isyana',
//     umur: 17,
//     isMarried: false,
//     alamat: {
//         rumah: 'Jl. Jonggrang',
//         kerja: 'Jl. Telkom',
//     },
//     arrGpa: [4.0, 4.0, 4.0],
// }

// studentN ,,

// 2. Function Declaration
function createStudentObj(nama, umur, isMarried) {
    let student = {};
    student.nama = nama;
    student.umur = umur;
    student.isMarried = isMarried;
    return student
};

const Yeye = createStudentObj('yeye', 17, true);
const Andre = createStudentObj('Andre', 20, true);


// 3. Contructor (keyword new, this)
function Student(nama, umur, isMarried) {
    // let this = {};
    this.nama = nama;
    this.umur = umur;
    this.isMarried = isMarried;
    // return this
}

const Tama = new Student('Tama', 9, true);
const Rudy = new Student('Rudy', 17, true);

// 4. Class (keyword Class, constructor, this)
class Person {
    constructor(nama, umur) {
        this.nama = nama;
        this.umur = umur;
    }
}

const Sudarman = new Person('Sudarman', 20);

// 5. Object.create()
// 6. Object.assign()
// 7. Rest/ spread operator (titk tiga ...) (ES6) 
// 8. dll

